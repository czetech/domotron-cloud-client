<?php

namespace DomotronCloudClient\Model\Collection;

abstract class Collection
{
    /** @var string */
    protected $itemClass;

    /** @var array */
    protected $items = [];

    /** @var array */
    protected $pagination = [];

    /**
     * @param array $items
     * @param array $pagination
     */
    public function __construct(array $items, array $pagination)
    {
        foreach ($items as $item) {
            if ($item instanceof $this->itemClass) {
                $this->items[] = $item;
            }
        }

        $this->pagination = [
            'total' => isset($pagination['total']) ? (int) $pagination['total'] : null,
            'count' => isset($pagination['count']) ? (int) $pagination['count'] : null,
            'per_page' => isset($pagination['per_page']) ? (int) $pagination['per_page'] : null,
            'current_page' => isset($pagination['current_page']) ? (int) $pagination['current_page'] : null,
            'total_pages' => isset($pagination['total_pages']) ? (int) $pagination['total_pages'] : null
        ];
    }

    /**
     * Returns total items in collection
     * @return int|null
     */
    public function getTotalItems()
    {
        return $this->pagination['total'];
    }

    /**
     * Returns total collection total pages
     * @return int|null
     */
    public function getTotalPages()
    {
        return $this->pagination['total_pages'];
    }

    /**
     * Returns current loaded items count in collection
     * @return int
     */
    public function getCurrentCount()
    {
        return count($this->items);
    }

    /**
     * Returns current collection page
     * @return int|null
     */
    public function getCurrentPage()
    {
        return $this->pagination['current_page'];
    }

    /**
     * Returns items per page count (limit)
     * @return int|null
     */
    public function getPerPage()
    {
        return $this->pagination['per_page'];
    }

    /**
     * Returns link for next data set if any
     * @return string|null
     */
    public function getNextLink()
    {
        return isset($this->pagination['links']['next']) ? $this->pagination['link']['next'] : null;
    }

    /**
     * Returns link for previous data set if any
     * @return string|null
     */
    public function getPreviousLink()
    {
        return isset($this->pagination['links']['previous']) ? $this->pagination['link']['previous'] : null;
    }

    /**
     * Returns all loaded collection items
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Check if collection has next page
     * @return bool
     */
    public function hasNextPage()
    {
        return (bool) isset($this->pagination['links']['next']);
    }

    /**
     * Check if collection has previous page
     * @return bool
     */
    public function hasPreviousPage()
    {
        return (bool) isset($this->pagination['links']['previous']);
    }
}

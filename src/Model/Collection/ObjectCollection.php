<?php

namespace DomotronCloudClient\Model\Collection;

use DomotronCloudClient\Model\Item\Obj;

class ObjectCollection extends Collection
{
    protected $itemClass = Obj::class;
}

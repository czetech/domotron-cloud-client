<?php

namespace DomotronCloudClient\Model\Collection;

use DomotronCloudClient\Model\Item\Address;

class AddressCollection extends Collection
{
    protected $itemClass = Address::class;
}

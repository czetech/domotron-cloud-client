<?php

namespace DomotronCloudClient\Model\Collection;

use DomotronCloudClient\Model\Item\Market;

class MarketCollection extends Collection
{
    protected $itemClass = Market::class;
}

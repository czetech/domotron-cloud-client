<?php

namespace DomotronCloudClient\Model\Collection;

use DomotronCloudClient\Model\Item\Partner;

class PartnerCollection extends Collection
{
    protected $itemClass = Partner::class;
}

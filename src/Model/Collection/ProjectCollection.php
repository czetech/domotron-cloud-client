<?php

namespace DomotronCloudClient\Model\Collection;

use DomotronCloudClient\Model\Item\Project;

class ProjectCollection extends Collection
{
    protected $itemClass = Project::class;
}

<?php

namespace DomotronCloudClient\Model\Item;

abstract class Item
{
    /** @var array */
    protected $data = [];

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = isset($data['data']) ? $data['data'] : $data;
        $this->processData();
    }

    /**
     * Get data from item
     * @param string $key
     * @param null $default
     * @return mixed|null
     */
    public function get($key, $default = null)
    {
        if (!isset($this->data[$key])) {
            return $default;
        }

        return $this->data[$key];
    }

    /**
     * Process data in child classes
     */
    protected function processData()
    {

    }
}

<?php

namespace DomotronCloudClient\Model\Item;

class UserAddress extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['address'])) {
            $this->data['address'] = new Address($this->data['address']);
        }
    }
}

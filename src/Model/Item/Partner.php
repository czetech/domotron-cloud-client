<?php

namespace DomotronCloudClient\Model\Item;

class Partner extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['address'])) {
            $this->data['address'] = new Address($this->data['address']);
        }

        if (isset($this->data['partner_dph'])) {
            $partnerDph = $this->data['partner_dph'];
            $this->data['partner_dph'] = [];

            foreach ($partnerDph['data'] as $record) {
                $this->data['partner_dph'][] = new PartnerDph($record);
            }
        }
    }
}

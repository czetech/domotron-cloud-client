<?php

namespace DomotronCloudClient\Model\Item;

class UserPartner extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['partner'])) {
            $this->data['partner'] = new Partner($this->data['partner']);
        }
    }
}

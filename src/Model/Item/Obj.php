<?php

namespace DomotronCloudClient\Model\Item;

class Obj extends Item
{
    /**
     * Process data
     */
    protected function processData()
    {
        if (isset($this->data['market'])) {
            $this->data['market'] = new Market($this->data['market']);
        }

        if (isset($this->data['salesman'])) {
            $this->data['salesman'] = new Salesman($this->data['salesman']);
        }

        if (isset($this->data['projectManager'])) {
            $this->data['projectManager'] = new ProjectManager($this->data['projectManager']);
        }
    }
}

<?php

namespace DomotronCloudClient\Driver;

use DomotronCloudClient\Exception\AuthorizationException;
use DomotronCloudClient\Exception\BadResponseException;
use DomotronCloudClient\Model\Collection\AddressCollection;
use DomotronCloudClient\Model\Collection\MarketCollection;
use DomotronCloudClient\Model\Collection\ObjectCollection;
use DomotronCloudClient\Model\Collection\PartnerCollection;
use DomotronCloudClient\Model\Collection\ProjectCollection;
use DomotronCloudClient\Model\Item\Address;
use DomotronCloudClient\Model\Item\Market;
use DomotronCloudClient\Model\Item\Obj;
use DomotronCloudClient\Model\Item\Partner;
use DomotronCloudClient\Model\Item\Project;
use DomotronCloudClient\Model\Item\User;
use DomotronCloudClient\Queries;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use BadMethodCallException;

class HttpDriver extends Driver
{
    /** @var string */
    private $baseUrl;

    /** @var null|string */
    private $bearerToken;

    /** @var Client */
    private $client;

    /** @var Queries */
    private $queries;

    /**
     * @param string $url
     * @param string $bearerToken
     */
    public function __construct($url, $bearerToken = null)
    {
        $this->baseUrl = rtrim($url, '/');
        $this->bearerToken = $bearerToken;
        $this->client = new Client([
            'http_errors' => false
        ]);
    }

    /**
     * @param Queries $queries
     */
    public function setQueries(Queries $queries)
    {
        $this->queries = $queries;
    }

    /*****************************************************
     * User endpoint methods
     *****************************************************/


	    /**
	     * Fetch user by id
	     * @param int $id
	     * @param string|null $userToken
	     * @return User
	     * @throws AuthorizationException
	     * @throws BadResponseException
	     */
	    public function getUser($id, $userToken = null)
	    {
	        $data = $this->callGet('api/v1/user/full', $id, $userToken);
	        return new User($data['data']);
	    }

    /**
     * @param string $sessionId
     * @return int
     * @throws BadResponseException
     */
    public function getUserIdFromSession($sessionId)
    {
        $request = new Request('GET', $this->baseUrl . '/api/v1/user/session/' . sha1($sessionId));
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['result']['id'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['result']['id'];
    }

    /**
     * @param string $sessionId
     * @return int
     * @throws BadResponseException
     */
    public function extendSession($sessionId)
    {
        $request = new Request('POST', $this->baseUrl . '/api/v1/user/session/' . sha1($sessionId));
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['result']['valid_until'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['result']['valid_until'];
    }

    /**
     * @param string $sessionId
     * @return bool
     * @throws BadResponseException
     */
    public function invalidateSession($sessionId)
    {
        $request = new Request('DELETE', $this->baseUrl . '/api/v1/user/session/' . sha1($sessionId));
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['result'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['result'] === 'ok';
    }

    /**
     * @param int $userId
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getUserData($userId)
    {
        $request = new Request('GET', $this->baseUrl . '/api/v1/user/' . $userId, [
            'Authorization' => 'Bearer ' . $this->bearerToken
        ]);
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['result'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['result'];
    }

    /**
     * Fetch user permissions
     * @param int $userId
     * @param array $keys
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getPermissions($userId, array $keys)
    {
        $request = new Request('GET', $this->baseUrl . '/api/v1/user/permissions/' . $userId . '?' . http_build_query([
                'key' => $keys
            ]), [
            'Authorization' => 'Bearer ' . $this->bearerToken
        ]);
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['result'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['result'];
    }

    /**
     * @param int $userId
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getAllRelatedUserProjects($userId)
    {
        $request = new Request('GET', $this->baseUrl . '/api/v1/user/project/' . $userId, [
            'Authorization' => 'Bearer ' . $this->bearerToken
        ]);
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return is_array($data['data']) ? $data['data'] : [];
    }

    /**
     * @param int $userId
     * @param int $projectId
     * @return bool
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function setUserFavoriteProject($userId, $projectId)
    {
        $request = new Request('POST', $this->baseUrl . '/api/v1/user/project/favorite', [
            'Authorization' => 'Bearer ' . $this->bearerToken
        ]);
        $options = ['json' => [
            'user_id' => $userId,
            'project_id' => $projectId
        ]];
        $this->addInfoToActiveQuery($request, $options);
        $response = $this->client->send($request, $options);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return (bool) $data['data'] == 'ok';
    }

    /*****************************************************
     * Market endpoint methods
     *****************************************************/

    /**
     * Fetch market by id
     * @param int $id
     * @param string|null $userToken
     * @return Market
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getMarket($id, $userToken = null)
    {
        $data = $this->callGet('api/v1/market', $id, $userToken);
        return new Market($data['data']);
    }

    /**
     * Fetch market collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return MarketCollection
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function listingMarkets($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $data = $this->callListing('api/v1/market', $page, $limit, $search, $searchIn, $userToken);
        $result = [];
        foreach ($data['data'] as $market) {
            $result[] = new Market($market);
        }

        return new MarketCollection($result, $data['meta']['pagination']);
    }

    /**
     * Create new market
     * @param array $data
     * @param string|null $userToken
     * @return Market
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function createMarket(array $data, $userToken = null)
    {
        $data = $this->callCreate('api/v1/market', $data, $userToken);
        return new Market($data['data']);
    }

    /**
     * Update existing market
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Market
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function updateMarket($id, array $data, $userToken = null)
    {
        $this->callUpdate('api/v1/market', $id, $data, $userToken);
        return new Market($data['data']);
    }

    /*****************************************************
     * Object endpoint methods
     *****************************************************/

    /**
     * Fetch object by id
     * @param int $id
     * @param string|null $userToken
     * @return Obj
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getObject($id, $userToken = null)
    {
        $data = $this->callGet('api/v1/object', $id, $userToken);
        return new Obj($data['data']);
    }

    /**
     * Fetch object collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ObjectCollection
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function listingObjects($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $data = $this->callListing('api/v1/object', $page, $limit, $search, $searchIn, $userToken);
        $result = [];
        foreach ($data['data'] as $market) {
            $result[] = new Obj($market);
        }

        return new ObjectCollection($result, $data['meta']['pagination']);
    }

    /**
     * Create new object
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function createObject(array $data, $userToken = null)
    {
        $data = $this->callCreate('api/v1/object', $data, $userToken);
        return new Obj($data['data']);
    }

    /**
     * Update existing object
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function updateObject($id, array $data, $userToken = null)
    {
        $data = $this->callUpdate('api/v1/object', $id, $data, $userToken);
        return new Obj($data['data']);
    }

    /*****************************************************
     * Project endpoint methods
     *****************************************************/

    /**
     * Fetch project by id
     * @param int $id
     * @param string|null $userToken
     * @return Project
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getProject($id, $userToken = null)
    {
        $data = $this->callGet('api/v1/project', $id, $userToken);
        return new Project($data['data']);
    }

    /**
     * Fetch project collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ProjectCollection
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function listingProjects($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $data = $this->callListing('api/v1/project', $page, $limit, $search, $searchIn, $userToken);
        $result = [];
        foreach ($data['data'] as $market) {
            $result[] = new Project($market);
        }

        return new ProjectCollection($result, $data['meta']['pagination']);
    }

    /**
     * Create new project
     * @param array $data
     * @param string|null $userToken
     * @return Project
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function createProject(array $data, $userToken = null)
    {
        $data = $this->callCreate('api/v1/project', $data, $userToken);
        return new Project($data['data']);
    }

    /**
     * Update existing project
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Project
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function updateProject($id, array $data, $userToken = null)
    {
        $data = $this->callUpdate('api/v1/project', $id, $data, $userToken);
        return new Project($data['data']);
    }

    /*****************************************************
     * Address endpoint methods
     *****************************************************/

    /**
     * Fetch address by id
     * @param int $id
     * @param string|null $userToken
     * @return Address
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getAddress($id, $userToken = null)
    {
        $data = $this->callGet('api/v1/address', $id, $userToken);
        return new Address($data['data']);
    }

    /**
     * Fetch address collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return AddressCollection
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function listingAddresses($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $data = $this->callListing('api/v1/address', $page, $limit, $search, $searchIn, $userToken);
        $result = [];
        foreach ($data['data'] as $market) {
            $result[] = new Address($market);
        }

        return new AddressCollection($result, $data['meta']['pagination']);
    }

    /**
     * Create new address
     * @param array $data
     * @param string|null $userToken
     * @return Address
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function createAddress(array $data, $userToken = null)
    {
        $data = $this->callCreate('api/v1/address', $data, $userToken);
        return new Address($data['data']);
    }

    /**
     * Update existing address
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Address
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function updateAddress($id, array $data, $userToken = null)
    {
        $data = $this->callUpdate('api/v1/address', $id, $data, $userToken);
        return new Address($data['data']);
    }

    /*****************************************************
     * Partner endpoint methods
     *****************************************************/

    /**
     * Fetch partner by id
     * @param int $id
     * @param string|null $userToken
     * @return Partner
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getPartner($id, $userToken = null)
    {
        $data = $this->callGet('api/v1/partner', $id, $userToken);
        return new Partner($data['data']);
    }

    /**
     * Fetch partner collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return PartnerCollection
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function listingPartners($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $data = $this->callListing('api/v1/partner', $page, $limit, $search, $searchIn, $userToken);
        $result = [];
        foreach ($data['data'] as $market) {
            $result[] = new Partner($market);
        }

        return new PartnerCollection($result, $data['meta']['pagination']);
    }

    /**
     * Create new partner
     * @param array $data
     * @param string|null $userToken
     * @return Partner
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function createPartner(array $data, $userToken = null)
    {
        $data = $this->callCreate('api/v1/partner', $data, $userToken);
        return new Partner($data['data']);
    }

    /**
     * Update existing partner
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Partner
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function updatePartner($id, array $data, $userToken = null)
    {
        $data = $this->callUpdate('api/v1/partner', $id, $data, $userToken);
        return new Partner($data['data']);
    }

    /*****************************************************
     * Agreement endpoint methods
     *****************************************************/

    /**
     * @param int $userId
     * @param string $type
     * @param int|null $foreignId
     * @return bool
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function getAgreement($userId, $type, $foreignId = null)
    {
        $headers = ['Authorization' => 'Bearer ' . $this->bearerToken];
        $params = [
            'user_id' => $userId,
            'type' => $type
        ];

        if ($foreignId) {
            $params['foreign_id'] = $foreignId;
        }

        $request = new Request('GET', $this->baseUrl . '/api/v1/agreement?' . http_build_query($params), $headers);
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data']['agreement'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return (bool) $data['data']['agreement'];
    }

    /**
     * @param int $userId
     * @param bool $agreed
     * @param string $type
     * @param int|null $foreignId
     * @param string|null $note
     * @return bool
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function setAgreement($userId, $agreed, $type, $foreignId = null, $note = null)
    {
        $headers = ['Authorization' => 'Bearer ' . $this->bearerToken];
        $params = [
            'user_id' => $userId,
            'agreed' => $agreed,
            'type' => $type
        ];

        if ($foreignId !== null) {
            $params['foreign_id'] = $foreignId;
        }

        if ($note !== null) {
            $params['note'] = $note;
        }

        $request = new Request('POST', $this->baseUrl . '/api/v1/agreement', $headers);
        $options = ['json' => $params];
        $this->addInfoToActiveQuery($request, $options);
        $response = $this->client->send($request, $options);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['status'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data['status'] === 'success';
    }

    /*****************************************************
     * Call methods
     *****************************************************/

    /**
     * Fetch data by id
     * @param string $relativeUrl
     * @param int $id
     * @param string|null $userToken
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    private function callGet($relativeUrl, $id, $userToken = null)
    {
        $request = new Request('GET', $this->baseUrl . '/' . trim($relativeUrl) . '/' . $id, $this->prepareHeaders($userToken));
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data;
    }

    /**
     * Fetch collection
     * @param string $relativeUrl
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function callListing($relativeUrl, $page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        $queryData = [
            'page' => $page,
            'limit' => $limit
        ];

        if ($search !== null) {
            if (!$searchIn) {
                throw new BadMethodCallException('Param $searchIn is mandatory if $search param is used');
            }

            $queryData = array_merge($queryData, [
                'search' => $search,
                'search_in' => $searchIn
            ]);
        }

        $request = new Request('GET', $this->baseUrl . '/' . trim($relativeUrl) . '?' . http_build_query($queryData), $this->prepareHeaders($userToken));
        $this->addInfoToActiveQuery($request);
        $response = $this->client->send($request);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data;
    }

    /**
     * Create new record
     * @param string $relativeUrl
     * @param array $data
     * @param string|null $userToken
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function callCreate($relativeUrl, array $data, $userToken = null)
    {
        $request = new Request('PUT', $this->baseUrl . '/' . trim($relativeUrl), $this->prepareHeaders($userToken));
        $options = ['json' => $data];
        $this->addInfoToActiveQuery($request, $options);
        $response = $this->client->send($request, $options);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data;
    }

    /**
     * Update existing record
     * @param string $relativeUrl
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return array
     * @throws AuthorizationException
     * @throws BadResponseException
     */
    public function callUpdate($relativeUrl, $id, array $data, $userToken = null)
    {
        $request = new Request('POST', $this->baseUrl . '/' . trim($relativeUrl) . '/' . $id, $this->prepareHeaders($userToken));
        $options = ['json' => $data];
        $this->addInfoToActiveQuery($request, $options);
        $response = $this->client->send($request, $options);

        if ($response->getStatusCode() === 403) {
            throw new AuthorizationException();
        }

        $data = json_decode($response->getBody(), true);
        if ($response->getStatusCode() !== 200) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        if (!isset($data['data'])) {
            throw (new BadResponseException($this->getErrorMessage($response->getStatusCode(), $data)))->setDebugData($data);
        }

        return $data;
    }

    /*****************************************************
     * Help methods
     *****************************************************/

    /**
     * @param string|null $userToken
     * @return array
     */
    private function prepareHeaders($userToken = null)
    {
        $headers = ['Authorization' => 'Bearer ' . $this->bearerToken];
        if ($userToken) {
            $headers['Authentication'] = sha1($userToken);
        }
        return $headers;
    }

    /**
     * Parse error message from result data
     * @param int $statusCode
     * @param mixed $data
     * @return string
     */
    private function getErrorMessage($statusCode, $data)
    {
        if (!is_array($data)) {
            return '';
        }

        if (isset($data['error'])) {
            return $data['error'];
        }

        if ($statusCode !== 200 && isset($data['message'])) {
            return $data['message'];
        }

        return '';
    }

    /**
     * @param Request $request
     * @param array $options
     */
    private function addInfoToActiveQuery(Request $request, array $options = [])
    {
        $this->queries->addInfoToActiveQuery([
            'request' => [
                'method' => $request->getMethod(),
                'url' => (string) $request->getUri(),
                'headers' => $request->getHeaders(),
                'body' => (string) $request->getBody()
            ],
            'options' => $options
        ]);
    }
}

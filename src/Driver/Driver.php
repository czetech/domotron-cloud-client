<?php

namespace DomotronCloudClient\Driver;

use DomotronCloudClient\Model\Collection\MarketCollection;
use DomotronCloudClient\Model\Collection\ObjectCollection;
use DomotronCloudClient\Model\Collection\ProjectCollection;
use DomotronCloudClient\Model\Item\Market;
use DomotronCloudClient\Model\Item\Obj;
use DomotronCloudClient\Model\Item\Project;
use DomotronCloudClient\Model\Item\User;

abstract class Driver
{
    /*****************************************************
     * User endpoint methods
     *****************************************************/

    /**
     * Fetch user by id
     * @param int $id
     * @param string|null $userToken
     * @return User
     */
    abstract public function getUser($id, $userToken = null);

    /**
     * @param string $sessionId
     * @return int
     */
    abstract public function getUserIdFromSession($sessionId);

    /**
     * @param string $sessionId
     * @return int
     */
    abstract public function extendSession($sessionId);

    /**
     * @param string $sessionId
     * @return bool
     */
    abstract public function invalidateSession($sessionId);

    /**
     * @param int $userId
     * @return array
     */
    abstract public function getUserData($userId);

    /**
     * Fetch user permissions
     * @param int $userId
     * @param array $keys
     * @return array
     */
    abstract public function getPermissions($userId, array $keys);

    /**
     * Fetch all user related plc
     * @param int $userId
     * @return array
     */
    abstract public function getAllRelatedUserProjects($userId);

    /**
     * Set user new favorite plc
     * @param int $userId
     * @param int $projectId
     * @return bool
     */
    abstract public function setUserFavoriteProject($userId, $projectId);

    /*****************************************************
     * Market endpoint methods
     *****************************************************/

    /**
     * Fetch market by id
     * @param int $id
     * @param string|null $userToken
     * @return Market
     */
    abstract public function getMarket($id, $userToken = null);

    /**
     * Fetch market collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return MarketCollection
     */
    abstract public function listingMarkets($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null);

    /**
     * Create new market
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    abstract public function createMarket(array $data, $userToken = null);

    /**
     * Update existing market
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    abstract public function updateMarket($id, array $data, $userToken = null);

    /*****************************************************
     * Object endpoint methods
     *****************************************************/

    /**
     * Fetch object by id
     * @param int $id
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function getObject($id, $userToken = null);

    /**
     * Fetch object collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ObjectCollection
     */
    abstract public function listingObjects($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null);

    /**
     * Create new object
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function createObject(array $data, $userToken = null);

    /**
     * Update existing object
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function updateObject($id, array $data, $userToken = null);

    /*****************************************************
     * Project endpoint methods
     *****************************************************/

    /**
     * Fetch project by id
     * @param int $id
     * @param string|null $userToken
     * @return Project
     */
    abstract public function getProject($id, $userToken = null);

    /**
     * Fetch project collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ProjectCollection
     */
    abstract public function listingProjects($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null);

    /**
     * Create new project
     * @param array $data
     * @param string|null $userToken
     * @return Project
     */
    abstract public function createProject(array $data, $userToken = null);

    /**
     * Update existing project
     * @param $id
     * @param array $data
     * @param string|null $userToken
     * @return Project
     */
    abstract public function updateProject($id, array $data, $userToken = null);

    /*****************************************************
     * Address endpoint methods
     *****************************************************/

    /**
     * Fetch address by id
     * @param int $id
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function getAddress($id, $userToken);

    /**
     * Fetch address collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ObjectCollection
     */
    abstract public function listingAddresses($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null);

    /**
     * Create new address
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function createAddress(array $data, $userToken = null);

    /**
     * Update existing address
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function updateAddress($id, array $data, $userToken = null);

    /*****************************************************
     * Partner endpoint methods
     *****************************************************/

    /**
     * Fetch partner by id
     * @param int $id
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function getPartner($id, $userToken = null);

    /**
     * Fetch partner collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return ObjectCollection
     */
    abstract public function listingPartners($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null);

    /**
     * Create new partner
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function createPartner(array $data, $userToken = null);

    /**
     * Update existing partner
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Obj
     */
    abstract public function updatePartner($id, array $data, $userToken = null);

    /*****************************************************
     * Agreement endpoint methods
     *****************************************************/

    /**
     * @param int $userId
     * @param string $type
     * @param int|null $foreignId
     * @return bool
     */
    abstract public function getAgreement($userId, $type, $foreignId = null);

    /**
     * @param int $userId
     * @param bool $agreed
     * @param string $type
     * @param int|null $foreignId
     * @param string|null $note
     * @return bool
     */
    abstract public function setAgreement($userId, $agreed, $type, $foreignId = null, $note = null);
}

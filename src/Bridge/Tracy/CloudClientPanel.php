<?php

namespace DomotronCloudClient\Bridge\Tracy;

use DomotronCloudClient\CloudClient;
use DomotronCloudClient\Driver\Driver;
use DomotronCloudClient\Queries;
use Tracy\IBarPanel;

class CloudClientPanel implements IBarPanel
{
    /** @var CloudClient */
    private $cloudClient;

    /** @var Queries */
    private $queries;

    /** @var Driver */
    private $driver;

    /**
     * @param CloudClient $cloudClient
     */
    public function __construct(CloudClient $cloudClient)
    {
        $this->cloudClient = $cloudClient;
        $cloudClient->registerPanel($this);
    }

    /**
     * @param Queries $queries
     */
    public function setQueries(Queries $queries)
    {
        $this->queries = $queries;
    }

    /**
     * @param Driver $driver
     */
    public function setDriver(Driver $driver)
    {
        $this->driver = $driver;
    }

    /**
     * Renders tab.
     * @return string
     */
    public function getTab()
    {
        if (headers_sent() && !session_id()) {
            return;
        }

        ob_start();
        $cloudClient = $this->cloudClient;
        $queries = $this->queries;
        require __DIR__ . '/templates/CloudClientPanel.tab.phtml';
        return ob_get_clean();
    }

    /**
     * Renders panel.
     * @return string
     */
    public function getPanel()
    {
        ob_start();
        $cloudClient = $this->cloudClient;
        $queries = $this->queries;
        $driver = $this->driver;
        require __DIR__ . '/templates/CloudClientPanel.panel.phtml';
        return ob_get_clean();
    }
}

<?php

namespace DomotronCloudClient\Exception;

class AuthorizationException extends DriverException
{
}

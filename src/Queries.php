<?php

namespace DomotronCloudClient;

class Queries
{
    /** @var array */
    private $queries = [];

    /** @var array */
    private $activeQuery;

    /**
     * Start logging new query
     * @param string $name
     * @param array $info
     */
    public function startQuery($name, array $info = [])
    {
        $this->activeQuery = [
            'name' => $name,
            'start' => microtime(true),
            'info' => $info
        ];
    }

    /**
     * Add info to active query
     * @param array $info
     */
    public function addInfoToActiveQuery(array $info = [])
    {
        if (!$this->activeQuery) {
            return;
        }

        $this->activeQuery['info'] = array_merge($this->activeQuery['info'], $info);
    }

    /**
     * Finish active query as success
     * @param mixed $result
     */
    public function finishQuerySuccess($result)
    {
        if (!$this->activeQuery) {
            return;
        }

        $this->queries[] = [
            'name' => $this->activeQuery['name'],
            'info' => $this->activeQuery['info'],
            'duration' => microtime(true) - $this->activeQuery['start'],
            'result' => $result,
            'error' => ''
        ];

        $this->activeQuery = null;
    }

    /**
     * Finish active query as error
     * @param mixed $error
     */
    public function finishQueryError($error)
    {
        if (!$this->activeQuery) {
            return;
        }

        $this->queries[] = [
            'name' => $this->activeQuery['name'],
            'info' => $this->activeQuery['info'],
            'duration' => microtime(true) - $this->activeQuery['start'],
            'result' => '',
            'error' => $error
        ];

        $this->activeQuery = null;
    }

    /**
     * Returns all finished queries
     * @return array
     */
    public function getQueries()
    {
        return $this->queries;
    }

    /**
     * Returns finished queries count
     * @return int
     */
    public function getQueriesCount()
    {
        return count($this->queries);
    }

    /**
     * Calculate finished queries duration in seconds
     * @return int
     */
    public function getQueriesDuration()
    {
        $result = 0;
        foreach ($this->queries as $query) {
            $result += $query['duration'];
        }
        return $result;
    }
}

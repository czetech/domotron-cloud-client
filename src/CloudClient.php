<?php

namespace DomotronCloudClient;

use DomotronCloudClient\Bridge\Tracy\CloudClientPanel;
use DomotronCloudClient\Driver\Driver;
use DomotronCloudClient\Endpoint\AddressEndpoint;
use DomotronCloudClient\Endpoint\AgreementEndpoint;
use DomotronCloudClient\Endpoint\MarketEndpoint;
use DomotronCloudClient\Endpoint\ObjectEndpoint;
use DomotronCloudClient\Endpoint\PartnerEndpoint;
use DomotronCloudClient\Endpoint\ProjectEndpoint;
use DomotronCloudClient\Endpoint\UserEndpoint;

class CloudClient
{
    /** @var Driver */
    private $driver;

    /** @var UserEndpoint */
    private $userEndpoint;

    /** @var MarketEndpoint */
    private $marketEndpoint;

    /** @var ObjectEndpoint */
    private $objectEndpoint;

    /** @var ProjectEndpoint */
    private $projectEndpoint;

    /** @var PartnerEndpoint */
    private $partnerEndpoint;

    /** @var AddressEndpoint */
    private $addressEndpoint;

    /** @var AgreementEndpoint */
    private $agreementEndpoint;

    /** @var Queries */
    private $queries;

    /**
     * @param Driver $driver
     */
    public function __construct(Driver $driver)
    {
        $this->driver = $driver;
        $this->queries = new Queries();

        if (method_exists($this->driver, 'setQueries')) {
            $this->driver->setQueries($this->queries);
        }
    }

    /**
     * @return UserEndpoint
     */
    public function userEndpoint()
    {
        if (!$this->userEndpoint) {
            $this->userEndpoint = new UserEndpoint($this->driver, $this->queries);
        }
        return $this->userEndpoint;
    }

    /**
     * @return MarketEndpoint
     */
    public function marketEndpoint()
    {
        if (!$this->marketEndpoint) {
            $this->marketEndpoint = new MarketEndpoint($this->driver, $this->queries);
        }
        return $this->marketEndpoint;
    }

    /**
     * @return ObjectEndpoint
     */
    public function objectEndpoint()
    {
        if (!$this->objectEndpoint) {
            $this->objectEndpoint = new ObjectEndpoint($this->driver, $this->queries);
        }
        return $this->objectEndpoint;
    }

    /**
     * @return ProjectEndpoint
     */
    public function projectEndpoint()
    {
        if (!$this->projectEndpoint) {
            $this->projectEndpoint = new ProjectEndpoint($this->driver, $this->queries);
        }
        return $this->projectEndpoint;
    }

    /**
     * @return PartnerEndpoint
     */
    public function partnerEndpoint()
    {
        if (!$this->partnerEndpoint) {
            $this->partnerEndpoint = new PartnerEndpoint($this->driver, $this->queries);
        }
        return $this->partnerEndpoint;
    }

    /**
     * @return AddressEndpoint
     */
    public function addressEndpoint()
    {
        if (!$this->addressEndpoint) {
            $this->addressEndpoint = new AddressEndpoint($this->driver, $this->queries);
        }
        return $this->addressEndpoint;
    }

    /**
     * @return AgreementEndpoint
     */
    public function agreementEndpoint()
    {
        if (!$this->agreementEndpoint) {
            $this->agreementEndpoint = new AgreementEndpoint($this->driver, $this->queries);
        }
        return $this->agreementEndpoint;
    }

    /**
     * @internal
     * @param CloudClientPanel $panel
     */
    public function registerPanel(CloudClientPanel $panel)
    {
        $panel->setQueries($this->queries);
        $panel->setDriver($this->driver);
    }
}

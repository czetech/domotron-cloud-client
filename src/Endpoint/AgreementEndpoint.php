<?php

namespace DomotronCloudClient\Endpoint;

class AgreementEndpoint extends Endpoint
{
    /**
     * @param int $userId
     * @param string $type
     * @param int|null $foreignId
     * @return bool
     */
    public function get($userId, $type, $foreignId = null)
    {
        return $this->wrapWithQueryProcess('getAgreement', function () use ($userId, $type, $foreignId) {
            return $this->driver->getAgreement($userId, $type, $foreignId);
        });
    }

    /**
     * @param int $userId
     * @param bool $agreed
     * @param string $type
     * @param int|null $foreignId
     * @param string|null $note
     * @return bool
     */
    public function set($userId, $agreed, $type, $foreignId = null, $note = null)
    {
        return $this->wrapWithQueryProcess('setAgreement', function () use ($userId, $agreed, $type, $foreignId, $note) {
            return $this->driver->setAgreement($userId, $agreed, $type, $foreignId, $note);
        });
    }
}

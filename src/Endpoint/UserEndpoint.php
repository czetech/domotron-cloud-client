<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Item\User;

class UserEndpoint extends Endpoint
{
    /**
     * Fetch partner by id
     * @param int $id
     * @param string|null $userToken
     * @return User
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getUser', function () use ($id, $userToken) {
            return $this->driver->getUser($id, $userToken);
        });
    }

    /**
     * @param string $sessionId
     * @return int
     */
    public function getUserIdFromSession($sessionId)
    {
        return $this->wrapWithQueryProcess('getUserIdFromSession', function () use ($sessionId) {
            return $this->driver->getUserIdFromSession($sessionId);
        });
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getUserData($userId)
    {
        return $this->wrapWithQueryProcess('getUserData', function () use ($userId) {
            return $this->driver->getUserData($userId);
        });
    }

    /**
     * @param int $userId
     * @param array $keys
     * @return array
     */
    public function getPermissions($userId, array $keys)
    {
        return $this->wrapWithQueryProcess('getPermissions', function () use ($userId, $keys) {
            return $this->driver->getPermissions($userId, $keys);
        });
    }

    /**
     * @param int $userId
     * @return array
     */
    public function getAllRelatedUserProjects($userId)
    {
        return $this->wrapWithQueryProcess('getAllRelatedUserProjects', function () use ($userId) {
            return $this->driver->getAllRelatedUserProjects($userId);
        });
    }

    /**
     * @param int $userId
     * @param int $projectId
     * @return bool
     */
    public function setUserFavoriteProject($userId, $projectId)
    {
        return $this->wrapWithQueryProcess('setUserFavoriteProject', function () use ($userId, $projectId) {
            return $this->driver->setUserFavoriteProject($userId, $projectId);
        });
    }

    /**
     * @param string $sessionId
     * @return bool
     */
    public function invalidateSession($sessionId)
    {
        return $this->wrapWithQueryProcess('invalidateSession', function () use ($sessionId) {
            return $this->driver->invalidateSession($sessionId);
        });
    }

    /**
     * @param string $sessionId
     * @return int
     */
    public function extendSession($sessionId)
    {
        return $this->wrapWithQueryProcess('extendSession', function () use ($sessionId) {
            return $this->driver->extendSession($sessionId);
        });
    }
}

<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Driver\Driver;
use DomotronCloudClient\Queries;
use Exception;

class Endpoint
{
    /** @var Driver */
    protected $driver;

    /** @var Queries */
    protected $queries;

    /**
     * @param Driver $driver
     * @param Queries $queries
     */
    public function __construct(Driver $driver, Queries $queries)
    {
        $this->driver = $driver;
        $this->queries = $queries;
    }

    /**
     * Main query wrapper function
     * @param string $queryName
     * @param callable $callback
     * @return mixed
     * @throws Exception
     */
    protected function wrapWithQueryProcess($queryName, callable $callback)
    {
        $this->queries->startQuery($queryName);

        try {
            $result = $callback();
        } catch (Exception $e) {
            $this->finishQueryWithException($e);
            throw $e;
        }
        $this->queries->finishQuerySuccess($result);
        return $result;
    }

    /**
     * @param Exception $e
     */
    protected function finishQueryWithException(Exception $e)
    {
        $this->queries->finishQueryError([
            'error' => get_class($e),
            'message' => $e->getMessage()
        ]);
    }
}

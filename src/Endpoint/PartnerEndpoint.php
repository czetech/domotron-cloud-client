<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Collection\MarketCollection;
use DomotronCloudClient\Model\Item\Market;

class PartnerEndpoint extends Endpoint
{
    /**
     * Fetch partner by id
     * @param int $id
     * @param string|null $userToken
     * @return Market
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getPartner', function () use ($id, $userToken) {
            return $this->driver->getPartner($id, $userToken);
        });
    }

    /**
     * Fetch partner collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return MarketCollection
     */
    public function listing($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        return $this->wrapWithQueryProcess('listingPartners', function () use ($page, $limit, $search, $searchIn, $userToken) {
            return $this->driver->listingPartners($page, $limit, $search, $searchIn, $userToken);
        });
    }

    /**
     * Create new partner
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function create(array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('createPartner', function () use ($data, $userToken) {
            return $this->driver->createPartner($data, $userToken);
        });
    }

    /**
     * Update existing partner
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function update($id, array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('updatePartner', function () use ($id, $data, $userToken) {
            return $this->driver->updatePartner($id, $data, $userToken);
        });
    }
}

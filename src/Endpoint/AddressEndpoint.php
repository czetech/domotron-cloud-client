<?php

namespace DomotronCloudClient\Endpoint;

use DomotronCloudClient\Model\Collection\MarketCollection;
use DomotronCloudClient\Model\Item\Market;

class AddressEndpoint extends Endpoint
{
    /**
     * Fetch address by id
     * @param int $id
     * @param string|null $userToken
     * @return Market
     */
    public function get($id, $userToken = null)
    {
        return $this->wrapWithQueryProcess('getAddress', function () use ($id, $userToken) {
            return $this->driver->getAddress($id, $userToken);
        });
    }

    /**
     * Fetch address collection
     * @param int $page
     * @param int $limit
     * @param string|null $search
     * @param array|null $searchIn
     * @param string|null $userToken
     * @return MarketCollection
     */
    public function listing($page = 1, $limit = 10, $search = null, array $searchIn = null, $userToken = null)
    {
        return $this->wrapWithQueryProcess('listingAddresses', function () use ($page, $limit, $search, $searchIn, $userToken) {
            return $this->driver->listingAddresses($page, $limit, $search, $searchIn, $userToken);
        });
    }

    /**
     * Create new address
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function create(array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('createAddress', function () use ($data, $userToken) {
            return $this->driver->createAddress($data, $userToken);
        });
    }

    /**
     * Update existing address
     * @param int $id
     * @param array $data
     * @param string|null $userToken
     * @return Market
     */
    public function update($id, array $data, $userToken = null)
    {
        return $this->wrapWithQueryProcess('updateAddress', function () use ($id, $data, $userToken) {
            return $this->driver->updateAddress($id, $data, $userToken);
        });
    }
}

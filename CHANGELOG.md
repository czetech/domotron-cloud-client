# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.5.0] - 2021-02-27

## [1.4.0] - 2019-03-29

### Added

- Get metóda do userEndpointu pre získanie objektu usera podľa jeho ID. Metóda getUserData zostala naschvál oddelená a slúži len pre cloudUser libku

## [1.3.0] - 2018-10-09

### Changed

- metóda getAllUserPlc() zmenená na getAllRelatedUserProjects()
- metóda setUserFavoritePlc() zmenená na setUserFavoriteProject()

## [1.2.0] - 2018-09-11

### Added

- Volanie getAllUserPlc do UserEndpointu
- Volanie setUserFavoritePlc do UserEndpointu

### Fixed

- Presunuty guzzle do required z required-dev v composer.json

## [1.1.0] - 2018-09-03

### Added

- Agreement endpoint

## [1.0.0] - 2018-08-09

### Fixed

- Spracovanie partner modelu
- Pridaný partnerDph model

## [0.3.0] - 2018-08-09

### Added

- Endpointy pre adresu a partnera
- Do volaní ktoré sú ovplivnené userom pridaný paramenter $userToken ktorý orezáva výsledok len na oprávnené veci pre usera ktorému patrí daný token
- Možnosť v listing() metódach vyhľadávať podľa zvolených parametrov

## [0.2.0] - 2018-07-10

### Added

- BadResponseException rozšírené o metódu **getDebugData()**

## [0.1.0] - 2018-06-20

### Added

- Initial version

[Unreleased]: https://gitlab.com/domotron/cloud-client/compare/1.5.0...master
[1.5.0]: https://gitlab.com/domotron/cloud-client/compare/1.4.0...1.5.0
[1.4.0]: https://gitlab.com/domotron/cloud-client/compare/1.3.0...1.4.0
[1.3.0]: https://gitlab.com/domotron/cloud-client/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/domotron/cloud-client/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/domotron/cloud-client/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/domotron/cloud-client/compare/0.3.0...1.0.0
[0.3.0]: https://gitlab.com/domotron/cloud-client/compare/0.2.0...0.3.0
[0.2.0]: https://gitlab.com/domotron/cloud-client/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/domotron/cloud-client/tree/0.1.0

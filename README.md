# Cloud client

Domotron Cloud client pre jednoduchú komunikáciu s Domotron Cloudom.

### Nastavenie
Aktuálne je k dispozíci len HTTP driver ktorý potrebuje pre svoje fungovanie API token registrovaný na Cloude.

```php
use DomotronCloudClient\Driver\HttpDriver;
use DomotronCloudClient\CloudClient;

$httpDriver = new HttpDriver('http://cloud.mydomotron.com', 'token123');
$cloudClient = new CloudClient($httpDriver);
```

### Použitie
```php
// Dostupné endpointy
$cloudClient->userEndpoint(); // Slúži hlavne pre účely CloudUser libky
$cloudClient->marketEndpoint();
$cloudClient->objectEndpoint();
$cloudClient->projectEndpoint();
$cloudClient->addressEndpoint();
$cloudClient->partnerEndpoint();

// Endpointy market, object a project obsahujú rovnaké 4 metódy
$marketEndpoint->get($id, $userToken = null); // Načíta market podla ID
$marketEndpoint->listing($page, $limit, $search = null, array $searchIn = null, $userToken = null); // Načíta kolekciu marketov
$marketEndpoint->create(array $data, $userToken = null); // Vytvorí nový market
$marketEndpoint->update($id, array $data, $userToken = null); // Update market
```

### Vyhľadávanie
Vyhľadávanie je možné pomocou listing() endpointov. Parameter <code>$search</code>  definuje hľadaný vyraz.
Parameter <code>array $searchIn</code> definuje v ktorých poliach daného endpointu sa má hľadaný výraz vyhľadávať.

### Použitie endpointov ako "používateľ"
Endpointy ktoré je možné používať ako používateľ pomocou user tokenu:
- market
- object
- project
- address
- partner
- user

Do metód týchto endpointov je možné posielať parameter $userEndpoint (hodnota uložená v cookies pre prihláseného používateľa).
Výsledok týchto metód je následne upravený vzhľadom na oprávnenia používateľa ktorému token patrí. Napr. teda listing obsahuje len záznamy ku ktorým má prístup,
úprava záznamu bude úspešná len v prípade že používateľ má právo záznam upravovať atď.

**$userToken pre aktuálne prihláseného používateľa vieme získať pomocou volania $cloudUser->getCookieTokenValue()** (Potrebná CloudUser libka)

### Debug
#### Http Driver
Pre debuggovanie chýb pri použití HTTPDrivera môžeme na BadResponseException zavolať metódu **getDebugData()** ktorá nám
vráti data ktoré prišli ako odpoveď z HTTP volania. **Táto metóda slúži len na debugging! Rôzne driver-y môžu vracať rôzne dáta a obsah týchto dát sa môže bez upozornenia zmeniť.**

### Rozšírenia
#### Tracy Bar

Registrácia do Tracy baru v neon súbore:
```php
tracy:
	bar:
		- DomotronCloudClient\Bridge\Tracy\CloudClientPanel
```
